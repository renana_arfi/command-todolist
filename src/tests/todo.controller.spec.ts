import { create } from "../controller/todo.controller";
import { Note } from "../types/todo.interface";
import { expect } from "chai";

describe(`the controller todo tests`, () => {
    context("#create", ()=>{ 

    it(`should be a function`, () => {
        expect(create).to.be.a("function");
        expect(create).to.be.instanceOf(Function);
    });
   
    it(`should return arrNots with new note with the input value`, async () => {
        const arrNote :Note[]= [{ idNote: 'tx92u', value: 'shooping', complete: false }];
        const value ="sleep";
        await create(arrNote, value);
        const newNote = arrNote.find( (item: Note) => item.value === value);
        expect(newNote?.value).to.equal(value)
     });

    });
});