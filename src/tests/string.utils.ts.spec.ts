import { uniqeID } from "../utils/string.utils";
import { expect } from "chai";

describe(`the uniqeID tests`, () => {

    it(`should be a function`, () => {
        expect(uniqeID).to.be.a("function");
        expect(uniqeID).to.be.instanceOf(Function);
    });
   
    it(`should return uniqeID with 5 chars`, async () => {
         expect(uniqeID().length).to.equal(5);
     });
});