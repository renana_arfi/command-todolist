interface CommandObj {
  [commandName: string]: string[];
}
export const commands: CommandObj = {
  create: [`-v`, `--value`, `-b`, `--batch`],
  update: [`-id`],
  read: [`-a`, `--all`, `-c`, `--complete`, `-uc`, `--uncomplete`],
  remove: [`-id`, `-c`, "--complete"],
  help: [],
};
//help guide
export const usageText = `
    todo helps you manage you todo tasks.
  
    usage:
      todo <command> [<args>]
  
      commands can be:
      create
          description:  used to create a new todo 
          params: --value | -v
          usage:   todos add --value="buymilk buybread"
      update
          description:  used to toggles a todo completed state
          params: -id 
          usage:   todos update -id="jhdjdzh"
      remove
          description:  used to removes a todo from your list or to remove all completed tasks
          params: -id | -c | --complete
          usage:   todos remove -id="jhdjdzh"
      read
          description:  used to retrieve your todos + [all/complete/uncomplete]
          params: -a | --all | -c | --complete | -uc | --uncompleted
          usage:   todos read -a 
      help:        
          description: used to print the usage guide
          usage:   todos help 


          
    `;
