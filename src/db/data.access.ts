import fsp from 'fs/promises';
import fs from 'fs';
import { Note } from '../types/todo.interface.js';

export async function get_arrNote(): Promise<Note[]> {
    try {
        //if todo.json didnt exist create it
        if (!fs.existsSync('./todolist.json')) {
            fs.writeFileSync('./todolist.json', JSON.stringify([]))
        }
        
        // read todo.json 
        let data = await fsp.readFile(`./todolist.json`, 'utf-8');
        let dataNote: Note[] = JSON.parse(data);
        // creat arrNots and move there data 
        let arrNots = dataNote || [];
        // return arr
        
        return arrNots;
    } catch (error) {
        console.log(error);
        throw error;
    }
}

export async function save(arrNots: Note[]) {
    // save arrNots to todo.json 
    await fsp.writeFile(`./todolist.json`, JSON.stringify(arrNots, null, 2), 'utf-8')
    // show(arrNots)
}